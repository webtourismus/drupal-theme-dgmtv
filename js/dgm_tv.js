(function ($, Drupal, drupalSettings, once) {

  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.glide = drupalSettings.wt.glide || {};
  drupalSettings.wt.glide.glide = drupalSettings.wt.glide.glide || [];
  drupalSettings.wt.glide.options = drupalSettings.wt.glide.options || {};
  drupalSettings.wt.glide.options.type = drupalSettings.wt.glide.options.type || 'carousel';
  drupalSettings.wt.glide.options.focusAt = drupalSettings.wt.glide.options.focusAt ?? 'center';
  drupalSettings.wt.glide.options.hoverpause = drupalSettings.wt.glide.options.hoverpause ?? false;
  drupalSettings.wt.glide.options.autoplay = drupalSettings.wt.glide.options.autoplay ?? 6000;


  Drupal.behaviors.wtGlide = Drupal.behaviors.wtGlide || {
    attach: function (context, settings) {
      once('wtGlide', '.glide', context).forEach(elem => {
        let glide = new Glide(elem, drupalSettings.wt.glide.options);
        let timerbar = document.querySelector('.pagefoot__timer');
        glide.on(['build.after'], function() {
          let $current = $('.glide__bullet--0');
          timerbar.style.animationDuration = (($current.attr('data-duration') - 400) / 1000) + 's';
        });
        glide.on(['run.before'], function() {
          $('.glide__bullet').removeClass('is-active');

          let $currentVideo = $('.glide__slide--active .dgmvideo__container');
          if ($currentVideo.length >= 1) {
            drupalSettings.wt.youtube.players[$currentVideo.attr('id')].stopVideo();
          }
        });
        glide.on(['run.after'], function(obj) {
          let $currentBullet = $('.glide__bullet--' + glide.index);
          $currentBullet.addClass('is-active');
          timerbar.style.animationName = 'none';
          timerbar.style.animationDuration = (($currentBullet.attr('data-duration') - 400) / 1000) + 's';
          timerbar.style.color = '#ffffff';
          timerbar.offsetHeight; // trigger reflow
          timerbar.style.animationName = null;

          let $currentVideo = $('.glide__slide--active .dgmvideo__container');
          if ($currentVideo.length >= 1) {
            drupalSettings.wt.youtube.players[$currentVideo.attr('id')].playVideo();
          }
        })

        glide.mount();
        drupalSettings.wt.glide.glide.push( {'node': this, 'glide': glide} );
      });
    }
  }

  Drupal.behaviors.wtGlideBullets = Drupal.behaviors.wtGlideBullets || {
    attach: function (context, settings) {
      let $elements = $(once('wtGlideBullets', '.glide__bullet', context));
      $elements.each( function() {
        $(this).on('click', function() {
          drupalSettings.wt.glide.glide[0].glide.go('=' + $(this).attr('data-index'));
        });
      });
    }
  }

  Drupal.behaviors.wt_dgm_refresh_startpage = Drupal.behaviors.wt_dgm_refresh_startpage || {
    attach: function (context, settings) {
      var defaultTimeout = 120 * 60 * 1000;
      var variantTimeout = 999999999999;
      var timeout = defaultTimeout;
      window.setTimeout(function() {
        if (drupalSettings.wt.daytime.enabled) {
          var now = new Date();
          var start = new Date();
          var startTimeout = 999999999999;
          start.setHours(drupalSettings.wt.daytime.night_start.split(':')[0]);
          start.setMinutes(drupalSettings.wt.daytime.night_start.split(':')[1]);
          if (start > now) {
            startTimeout = start.getTime() - now.getTime();
          }
          var end = new Date();
          var endTimeout = 999999999999;
          end.setHours(drupalSettings.wt.daytime.night_end.split(':')[0]);
          end.setMinutes(drupalSettings.wt.daytime.night_end.split(':')[1]);
          if (end > now) {
            endTimeout = end.getTime() - now.getTime();
          }
          variantTimeout = Math.min(startTimeout, endTimeout);
        }
        timeout = Math.min(defaultTimeout, variantTimeout);
        window.location.href = '/';
        console.info(timeout);
      }, timeout);
    }
  };

  Drupal.behaviors.wt_dgm_override_pagetitle = Drupal.behaviors.wt_dgm_override_pagetitle || {
    attach: function (context, settings) {
      let $elements = $(once('wt_dgm_override_pagetitle', '.js-override-title', context));
      $elements.each(function () {
        /** views contextual filter title  title  doesn't work with drupal title block */
        let title = $(this).html();
        $('.pagehead h1').html( title );
      });
    }
  };

}(jQuery, Drupal, drupalSettings, once));
