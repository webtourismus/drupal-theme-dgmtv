(function ($, Drupal, drupalSettings, once) {

  Drupal.behaviors.wt_dgm_ticker = Drupal.behaviors.wt_dgm_ticker || {
    attach: function (context, settings) {
      let $elements = $(once('wt.dgm.wt_dgm_ticker', '.pagefoot__ticker', context));
      $elements.marquee({
        speed: 100
      });
    }
  };


}(jQuery, Drupal, drupalSettings, once));
