(function ($, Drupal, drupalSettings) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.youtube = drupalSettings.wt.youtube || {};
  drupalSettings.wt.youtube.ids = drupalSettings.wt.youtube.ids || [];
  drupalSettings.wt.youtube.players = drupalSettings.wt.youtube.players || {};

  Drupal.wtYoutube = Drupal.wtYoutube || {};
  Drupal.wtYoutube.createPlayers = Drupal.wtYoutube.createPlayers || function () {
    let possiblyClonedVideoContainer = document.querySelectorAll('[data-youtube-id]');
    possiblyClonedVideoContainer.forEach( function(slide, index) {
      let player = new YT.Player(slide, {
        height: document.querySelector('.paragraph--dgmvideo').clientHeight,
        width: window.innerWidth,
        videoId: slide.getAttribute('data-youtube-id'),
      });
      drupalSettings.wt.youtube.players[player.getIframe().id] = player;
    });
  }
} (jQuery, Drupal, drupalSettings));


function onYouTubeIframeAPIReady() {
  Drupal.wtYoutube.createPlayers();
}
